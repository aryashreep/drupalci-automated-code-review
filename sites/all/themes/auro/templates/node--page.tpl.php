<?php
/**
 * @file
 * Override the theme template for Basic content type.
 */
?>
<div id="node-<?php print $node->nid; ?>" class="<?php print $classes; ?>">
	<div class="node-inner">
		<div class="postBox">
			<div class="postBoxTop"></div>
			<div class="postBoxMid">
				<div class="postBoxMidInner first clearfix">
					<div class="date">
					<span class="month"><?php echo date("M", $node->created); ?></span>
					<br class="clear"/>
					<span class="day"><?php  echo date("j", $node->created); ?></span>
					<br class="clear"/>
					<span class="year"><?php echo date("Y", $node->created); ?></span>
					</div>
					<?php if (!empty($content['field_tags'])): ?>
					<div class="category">
					<?php print render($content['field_tags']); ?>
					<?php if($page): ?>
						<div class="postTags">
						<?php
                          print render($content['field_tags']);
                        ?>
						</div>
					<?php endif;?>	
					</div>			  
					<?php endif;?>
     				<h1<?php print $title_attributes; ?>><a href="<?php print $node_url; ?>"><?php print $title; ?></a></h1>
					<?php if ($page): ?>
					<div class="postMetaSingle">
					<img alt="Author" src="<?php print base_path() . path_to_theme();?>/images/ico_author.png"> Article by <?php print token_replace('<a href="../user/[node:author:uid]" title="Contact [node:author]">' . $name . '</a>', array('node' => $node)); ?>&nbsp;&nbsp;&nbsp;
					</div>
					<?php endif; ?>
					<div class="textPreview">
                    <?php
                      // We hide the tags, comments and links.
                      hide($content['comments']);
                      hide($content['links']);
                      hide($content['field_tags']);
                      print render($content);
                    ?>
					</div>
					<?php if(!$page): ?>
					<div class="postMeta">
						<a class="more-link" href="<?php print $node_url; ?>">Read more &raquo;</a>
						<div class="metaRight">
							<img alt="Author" src="<?php print base_path() . path_to_theme();?>/images/ico_author.png"> Article by <?php print token_replace('<a href="../user/[node:author:uid]" title="Contact $name</a>' . $name . '</a>', array('node' => $node)); ?>
						</div>
					</div>
					<?php endif;?>
						
				</div>
			</div>
			<div class="postBoxBottom"></div>
		</div>
	</div>
</div>	
<?php print render($content['comments']); ?>
