========================
Introduction to Auro
========================

Auro is meant to be YOUR theme. The theme is a very flexible Drupal 
theme well suited for personal, portfolio, freelancers, entrepreneurs, 
food, travel, adventures, photography, fitness, corporate or any other 
awesome blog. The theme is SEO friendly with Schema compatible structure 
that will make Google love your website. Auro theme is also fast to 
loads providing additional SEO and usability boost.

Auro theme is perfect if you want a simple, smart, and flexible theme starter.

===============
Installation
===============
Install the Libraries module(https://www.drupal.org/project/libraries).
Download the Auro theme and unzip & place inside the 'themes' 
directory. eg: sites/all/libraries. 
Download the modernizr.js library(https://modernizr.com/download) 
and copy to /modernizr/ folder.
Download the selectivizr-min.js library
(http://selectivizr.com/downloads/selectivizr-1.0.2.zip)
and copy to /selectivizr/ folder.
Please go to Appearance(admin/appearance) you will find Auro theme. 
Please click on "Enable and set default".

===========================
What are the files for ?
===========================

- auro.info                 => provide informations about the theme,
                               like regions, css, settings, js ...
- block-system-main.tpl.php => template to edit the content
- block.tpl.php             => template to edit the blocks
- comment.tpl.php           => template to edit the comments
- node.tpl.php              => template to edit the nodes (in content)
- page.tpl.php              => template to edit the page
- node--page.tpl.php        => Themes only Page type nodes. Note that this
                               is different from page.tpl.php which controls 
                               the layout of the entire page including header,
                               sidebars and so on.
- node--page.tpl.php        => Themes only Article type nodes.
- template.php              => used to modify drupal's default behavior
                               before outputting HTML through the theme
- theme-settings            => used to create additional settings in the 
                               theme settings page

============
In /CSS
============

- default.css  => define default classes, browser resets and admin styles
- ie8.css      => used to debug IE8
- ie9.css      => used to debug IE9
- layout.css   => define the layout of the theme
- print.css    => define the way the theme look like when printed
- style.css    => contains some default font styles. you can add custom css
- tabs.css     => styles for the admin tabs (from ZEN)


========================
Changing the Layout
========================

The layout used in Auro is fairly similar to the Holy Grail method. It has been
tested on all major browser including IE (5>10), Opera, Firefox, Safari, Chrome.
The purpose of this method is to have a minimal markup for an ideal display.
For accessibility and search engine optimization, the best order to display a 
page is the following :

    1. header
    2. content
    3. sidebars
    4. footer

This is how the page template is buit in Auro, and it works in fluid and fixed 
layout. Refers to the notes in layout.css to see how to modify the layout.

________________________________________________________________________________

Thanks for using Auro, and remember to use the issue queue in drupal.org
for any question or bug report:

https://www.drupal.org/project/issues/2637820

Current maintainers:
* Aryashree Pritikrishna (aryashreep)
https://www.drupal.org/user/1469492 (http://eTechBuddy.com)
